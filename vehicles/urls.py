from django.urls import path

from vehicles.views import get_vehicles


urlpatterns = [
    path("vehicles", get_vehicles, name="get_vehicles"),
]
