from django.shortcuts import render

from vehicles.models import Vehicle


def get_vehicles(request):

    vehicles = Vehicle.objects.all()
    return render(request, "vehicle_list.html", context={
        "listofvehicles": vehicles
    })
