from django.db import models


class Vehicle(models.Model):
    AUTOMATIC = "Automatic"
    MANUAL = "Manual"
    TRANSMISSION_CHOICES = [
        (AUTOMATIC, "Automatic"),
        (MANUAL, "Manual")
    ]
    brand = models.CharField(max_length=100, null=False)
    model = models.CharField(max_length=100, null=False)
    year = models.PositiveIntegerField(null=False) 
    transmission = models.CharField(max_length=32, null=True, )

    def __str__(self):
        return f'{self.brand} {self.model}'